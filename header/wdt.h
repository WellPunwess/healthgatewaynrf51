/*
 * wdt.h
 *
 *  Created on: Mar 29, 2017
 *      Author: procyon
 */

#ifndef INCLUDES_WDT_H_
#define INCLUDES_WDT_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "app_error.h"
#include "nrf.h"
#include "nrf_soc.h"
#include "nrf_sdm.h"
#include "app_scheduler.h"
#include "nordic_common.h"


#define TX_WDT	1
#define RX_WDT	0
#define WDT_RUN_WHILE_SPEEP_BIT	0 //This bit Keep the watchdog running while the CPU is sleeping
#define WDT_RUN_WHILE_PUASED_BY_DEBUG_BIT	3 //This bit Keep the watchdog running while the CPU is sleeping

void wdt_init(void);
void wdt_reload_register(uint8_t register_no);

#endif /* INCLUDES_WDT_H_ */
