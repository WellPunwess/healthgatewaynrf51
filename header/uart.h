#ifndef _M_UART_H_
#define _M_UART_H_

#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "app_button.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "wdt.h"
#include "nrf_delay.h"


#define UART_BUFFER_SIZE        100

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

typedef struct
{
  uint8_t active_app;
  uint8_t rx_flag;
  char buff[UART_BUFFER_SIZE];
}
uart_var_t;

void uart_app_initialize(void);
uint8_t uart_put_string(char *str,uint8_t size);

#endif
