
#ifndef M_MAIN_H
#define M_MAIN_H

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "boards.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_hci.h"
#include "app_uart.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_db_discovery.h"
#include "ble_lbs_c.h"
#include "ble_conn_state.h"
#include "nrf_delay.h"
#include "wdt.h"
#include "uart.h"
#include "m_ble.h"
#include "nrf.h"

#define NRF_LOG_MODULE_NAME ""
#include "nrf_log.h"
#include "nrf_log_ctrl.h"



typedef struct
{
  uint8_t count;
  uint32_t sec;
}
rtc_var_t;

typedef struct
{
  uint8_t data[6];
}
m_data_t;

typedef struct
{
   uint8_t addr[6];
   uint8_t data[6];
   uint8_t rssi;
}
m_ble_slot_t;

#endif