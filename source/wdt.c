/*
 * wdt.c
 *
 *  Created on: Mar 29, 2017
 *      Author: procyon
 */
#include "wdt.h"
//#include "nrf_drv_wdt.h"
//#include "nrf_drv_clock.h"


void wdt_init(void)
{
	NRF_WDT->CONFIG = (1 << WDT_RUN_WHILE_SPEEP_BIT)| (1 << WDT_RUN_WHILE_PUASED_BY_DEBUG_BIT); 			//keep the watchdog running while the CPU is sleeping or debug.
	NRF_WDT->CRV = 60*32768;             //set CRV to be 60 second timeout.
	NRF_WDT->RREN = 0x02;  				//Enable reload register 0
	NRF_WDT->TASKS_START = 1;           //Start the Watchdog timer
	wdt_reload_register(TX_WDT);
	wdt_reload_register(RX_WDT);
}

void wdt_reload_register(uint8_t register_no)
{
	NRF_WDT->RR[register_no] = 0x6E524635;
	//NRF_WDT->RR[0] = 0x6E524635;
}

void wdt_disable(void)
{
	NRF_WDT->CONFIG = 0x00; 			//pause the watchdog while the CPU is sleeping or debug.
	NRF_WDT->CRV = 10*32768;             //set CRV to be 3 second timeout.
	NRF_WDT->RREN = 0x00;  				//Disable all reload register
	NRF_WDT->TASKS_START = 0;           //Stop the Watchdog timer
}

