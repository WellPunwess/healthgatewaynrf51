#include "uart.h"
#include "app_uart.h"
#define NRF_LOG_MODULE_NAME ""
#include "nrf_log.h"
#include "nrf_log_ctrl.h"

//static uart_var_t uart_var;
extern uint8_t remote_cmd_flag;
extern uint8_t remote_cmd_data[100];

void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[100];
    static uint8_t index = 0;
    //static uint16_t lenght = 0;
    //uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            
            UNUSED_VARIABLE(app_uart_get(&data_array[index])); // get all data and put into data_array[] and drop return value from function call.
            index++;

            if ((data_array[index - 1] == '\n'))
            {
                data_array[index++] = 0; 
                memcpy(&remote_cmd_data,&data_array,index);
                remote_cmd_flag = 1;
                index = 0;
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        case APP_UART_TX_EMPTY:
          break;
      
            
        default:
            break;
    }
}

/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */

void uart_app_initialize(void)
{
    uint32_t                     err_code;
    const app_uart_comm_params_t comm_params =
    {
        11,
        12,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud9600
    };

    APP_UART_FIFO_INIT( &comm_params,
                       256,
                       256,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
    
    
}

uint8_t uart_put_string(char *str,uint8_t size)
{
  uint8_t idx;
  
  for(idx=0;idx<size;idx++)
  {
    app_uart_put(str[idx]);
    NRF_LOG_INFO("%c",str[idx]);
  }
  nrf_delay_ms(5);
  return(0);
}

