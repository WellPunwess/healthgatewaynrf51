/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

/**
 * @brief BLE LED Button Service central and client application main file.
 *
 * This example can be a central for up to 8 peripherals.
 * The peripheral is called ble_app_blinky and can be found in the ble_peripheral
 * folder.
 */

#include "main.h"
#include "time.h"

time_t current_time = 0;  
time_t CheckTime = 1543881600;

m_device_addr_t preferred_whitelist[MAX_CONNECTED_BLE_DEVICE];
m_device_addr_t connected_whitelist[MAX_CONNECTED_BLE_DEVICE];


//static m_ble_slot_t ble_slot;
static rtc_var_t rtc_var;

uint8_t device_list[50][6];
//uint8_t targetAddr[50][6]; 
uint8_t targetMAC;
uint8_t mapping[8];
uint8_t data[8][8];
uint8_t disconnect_flag;
uint8_t tag_disconnect_flag;
uint8_t disconnect_addr[6];
uint8_t remote_cmd_flag;
uint8_t remote_cmd_data[100];


uint8_t temp_data[8];
uint8_t con_handle;


int8_t rssi_data_value;
int8_t rssi_data_conn_handle;
void hexstr_to_char(char* hexstr,char* chrs);
void remote_cmd_process(uint8_t* data);
void time_add_second(void)
{
  current_time++;
}

/** @brief Function to sleep until a BLE event is received by the application.
 */
static void power_manage(void)
{
    ret_code_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

APP_TIMER_DEF(rtcEngine);

uint8_t counter_200ms;
uint8_t flag_200ms = 0;
time_t hexStringToTime(char* hextime);

static void rtc_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context);
  rtc_var.count++;
  
  counter_200ms++;
  if(counter_200ms >=20)
  {
    counter_200ms = 0;
    flag_200ms = 1;
  }
  
  if(rtc_var.count > 100)
  {
    rtc_var.count = 0;
    rtc_var.sec++;
    time_add_second();
    
  }
}

uint8_t find_device_slot(uint8_t device_addr[6])
{
  uint8_t i;
  
  for(i=0;i<50;i++)
  {
    if((device_addr[0] == device_list[i][0]) &&
       (device_addr[1] == device_list[i][1]) &&
       (device_addr[2] == device_list[i][2]) &&
       (device_addr[3] == device_list[i][3]) &&
       (device_addr[4] == device_list[i][4]) &&
       (device_addr[5] == device_list[i][5]))
    {
      return(i);
    }
  }
  return(0xFF);
}

void delete_device_slot(uint8_t slot_no)
{
  uint8_t i;
  
  for(i=0;i<6;i++)
  {
    device_list[slot_no][i] = 0;
  }
}

uint8_t find_blank_slot(void)
{
  uint8_t i;
  
  for(i=0;i<50;i++)
  {
    if((device_list[i][0] == 0) &&
       (device_list[i][1] == 0) &&
       (device_list[i][2] == 0) &&
       (device_list[i][3] == 0) &&
       (device_list[i][4] == 0) &&
       (device_list[i][5] == 0))
    {
      return(i);
    }
  }
  return(0xFF);
}

void add_device_list(uint8_t device_addr[6])
{
  uint8_t slot_no,i;
  //char buff[30];
  
  //sprintf(buff,"Connected device address: %02X%02X%02X%02X%02X%02X\r\n",device_addr[0],device_addr[1],device_addr[2],device_addr[3],device_addr[4],device_addr[5]);
  //uart_put_string(buff,strlen(buff));
  slot_no = find_device_slot(device_addr);
  if(slot_no == 0xFF)   //This Address is not found in device list
  {
    slot_no = find_blank_slot();
    for(i=0;i<6;i++)
    {
      device_list[slot_no][i] = device_addr[i];
    }
  }
}

void delete_device_list(uint8_t device_addr[6])
{
  //uint8_t slot_no;
  //char buff[30];
  
  //sprintf(buff,"Disconnected device address: %02X%02X%02X%02X%02X%02X\r\n",device_addr[0],device_addr[1],device_addr[2],device_addr[3],device_addr[4],device_addr[5]);
  //uart_put_string(buff,strlen(buff));
  //nrf_delay_ms(10);
/*  
  slot_no = find_device_slot(device_addr);
  
  if(slot_no != 0xFF)   //This Address is found in device list
  {
    delete_device_slot(slot_no);
  }
*/
}

static void timer_initialize(void)
{
  APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
  app_timer_create(&rtcEngine, APP_TIMER_MODE_REPEATED, rtc_handler);
  app_timer_start(rtcEngine, 333, NULL);
}

void loadWLfromFlash(void)
{
  memcpy(&preferred_whitelist,(uint32_t*)0x7F000,48);
}

static void whitelist_initialize(void)
{
  
  loadWLfromFlash();
  m_ble_add_whitelist(preferred_whitelist);
}


#define APP_PA_PIN              	17
#define APP_LNA_PIN              	19


#define APP_CPS_PIN			6

#define APP_AMP_PPI_CH_ID_SET   0
#define APP_AMP_PPI_CH_ID_CLR   1
#define APP_AMP_GPIOTE_CH_ID    0

static void pa_lna_setup(void)
{
    uint32_t err_code;
    nrf_gpio_cfg_output(APP_CPS_PIN);
		nrf_gpio_pin_clear(APP_CPS_PIN); //enable
	  	nrf_gpio_cfg_output(APP_PA_PIN);
		nrf_gpio_pin_clear(APP_PA_PIN); //
	  	nrf_gpio_cfg_output(APP_LNA_PIN);
		nrf_gpio_pin_clear(APP_LNA_PIN); //		

    static ble_opt_t pa_lna_opts = {
        .common_opt = {
            .pa_lna = {
							
                .pa_cfg = {
                    .enable = 1,
                    .active_high = 1,
                    .gpio_pin = APP_PA_PIN
                },
							
								
                .lna_cfg = {
                    .enable = 1,
                    .active_high = 1,
                    .gpio_pin = APP_LNA_PIN
                },
								
                .ppi_ch_id_set = APP_AMP_PPI_CH_ID_SET,
                .ppi_ch_id_clr = APP_AMP_PPI_CH_ID_CLR,
                .gpiote_ch_id = APP_AMP_GPIOTE_CH_ID
            }
        }
    };
    NRF_GPIO->DIRSET |= (1 << APP_PA_PIN) | (1 << APP_LNA_PIN) ;
    err_code = sd_ble_opt_set(BLE_COMMON_OPT_PA_LNA, &pa_lna_opts);
    APP_ERROR_CHECK(err_code);

}



static void main_variable_initialize(void)
{
  uint8_t i,j;
  rtc_var.count = 0;
  rtc_var.sec = 0;
  disconnect_flag = 0;
  remote_cmd_flag = 0;
  tag_disconnect_flag = 0;
  
  for(i=0;i<50;i++)
  {
    for(j=0;j<6;j++)
    {
      device_list[i][j] = 0;
    }
  }
  whitelist_initialize();
  for(i=0;i<8;i++)
    {
      for(j=0;j<6;j++)
      {
        //targetAddr[i][j] = 0x00;
      }
    }
}

static void device_initialize(void)
{
    ret_code_t err_code;
    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    
    leds_init();
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, NULL);
    wdt_init();
    main_variable_initialize();
    timer_initialize();
    uart_app_initialize();
    //uart_put_string("NRF START\r\n",strlen("NRF START\r\n"));
    
    buttons_init();
    ble_stack_init();
    pa_lna_setup();
    db_discovery_init();
    lbs_c_init();

    // Start scanning for perherals and initiate connection to devices which
    // advertise.
    scan_start();

    // Turn on the LED to signal scanning.
    bsp_board_led_on(CENTRAL_SCANNING_LED);

    nrf_gpio_cfg_output(21);
    nrf_gpio_pin_set(21);
}




void sendWL(uint8_t index)
{
    uint8_t i;
    uint32_t deviceID[2]; deviceID[0] = NRF_FICR->DEVICEID[0]; deviceID[1] = NRF_FICR->DEVICEID[1]; 
    char buff[256];  
    if(index == 10)
    {
      for(i=0;i<MAX_CONNECTED_BLE_DEVICE;i++)
      {
        sprintf(buff,"WL%d:TB%02X%02X, %02X%02X%02X%02X%02X%02X\n",i,(uint8_t)deviceID[0],(uint8_t)deviceID[1],preferred_whitelist[i].addr[0],preferred_whitelist[i].addr[1],preferred_whitelist[i].addr[2],preferred_whitelist[i].addr[3],preferred_whitelist[i].addr[4],preferred_whitelist[i].addr[5]);
        uart_put_string(buff,strlen(buff));
      }
    }else
    {
        i = index;
        sprintf(buff,"WL%d:TB%02X%02X, %02X%02X%02X%02X%02X%02X\n",i,(uint8_t)deviceID[0],(uint8_t)deviceID[1],preferred_whitelist[i].addr[0],preferred_whitelist[i].addr[1],preferred_whitelist[i].addr[2],preferred_whitelist[i].addr[3],preferred_whitelist[i].addr[4],preferred_whitelist[i].addr[5]);
        uart_put_string(buff,strlen(buff));

    }
}

uint8_t hexstring2int(uint8_t ascii)
{
    if((ascii >= '0')&&(ascii <= '9'))
       return(ascii-'0');
    if((ascii >= 'A')&&(ascii <= 'F'))
       return(10+(ascii-'A'));
     return(0);
}

m_device_addr_t string2MAC (uint8_t* data)
{
    m_device_addr_t temp;
    temp.addr[0] = hexstring2int(data[0])*16 + hexstring2int(data[1]);
    temp.addr[1] = hexstring2int(data[2])*16 + hexstring2int(data[3]);
    temp.addr[2] = hexstring2int(data[4])*16 + hexstring2int(data[5]);
    temp.addr[3] = hexstring2int(data[6])*16 + hexstring2int(data[7]);
    temp.addr[4] = hexstring2int(data[8])*16 + hexstring2int(data[9]);
    temp.addr[5] = hexstring2int(data[10])*16 + hexstring2int(data[11]);
    return temp;
}
void hexstr_to_char(char* hexstr,char* chrs)
{
    size_t len = strlen(hexstr);
    
        
    size_t final_len = len / 2;
    //NRF_LOG_INFO("%s ",(uint32_t)hexstr);    
    //NRF_LOG_INFO("len = %d, ",final_len);
    for (size_t i=0, j=0; j<final_len; i+=2, j++)
    {
        chrs[j] = ((hexstr[i] % 32 + 9) % 25 * 16 + (hexstr[i+1] % 32 + 9) % 25);
        //chrs[j] = 'a';
        //NRF_LOG_INFO("%02x ",chrs[j]);
    }
    //NRF_LOG_INFO("\r\n");
    chrs[final_len] = '\0';
    //return (unsigned char*)chrs;
}



void nb_cmd_intrepred(uint8_t* data)
{
  char strSocket[20];
  char strIP[20];
  char strPORT[20];
  char strLenght[20];
  char strData[256];
  char strRemainingLenght[20];
  
  char strDataString[256];
  
  char temp[512];
  strcpy(temp,(char*)data);
  //NRF_LOG_INFO(">>INTREPRED %s\r\n",(uint32_t)temp);
   const char s[2] = ",";
   char *token;  
   uint8_t counter = 0;
   
   token = strtok((char*)temp, s);
    while( token != NULL ) 
    {
      //NRF_LOG_INFO("%d:%s\r\n",counter, (uint32_t)token);    
    
   
      switch (counter)
      {
        case 0:
          strcpy(strSocket,token);        
          break;
        case 1:
          strcpy(strIP,token);
          break;
        case 2:
          strcpy(strPORT,token);
          break;
        case 3:
          strcpy(strLenght,token);
          break;
        case 4:
          strcpy(strData,token);
          break;
        case 5:
          strcpy(strRemainingLenght,token);
          break;
        default:
          break;
      }
    
      token = strtok(NULL, s);      
      counter ++;
  }
  if(counter >=5)
  {
    
    hexstr_to_char((char*)strData,(char*)strDataString);
    //NRF_LOG_INFO("%s\r\n",(uint32_t)strDataString);
    remote_cmd_process((uint8_t*)strDataString);
    
  }
  
  
  
  
}

int8_t writeflash_countdown = -1;
int8_t hb_countdown = -1;
int8_t _hb_countdown = 1;
int8_t is_NB_IOT_wait_ack = 1;
char serialNO[30];
void nb_CGATT_1(void);
void nb_CGATT_0(void);
unsigned char a[100] = "";
void remote_cmd_process(uint8_t* data)
{
  
    uint32_t cmd_type = 0;
    uint8_t WL_index = 0;
    char buff[256];  
    uint32_t deviceID[2]; deviceID[0] = NRF_FICR->DEVICEID[0]; deviceID[1] = NRF_FICR->DEVICEID[1]; 
    //sprintf(buff,"debug - RECV cmd : %s",data);
    //uart_put_string(buff,strlen(buff));
    if(strlen((char*)data) > 2 )
    {
        NRF_LOG_INFO("nb(%d)>%s",strlen((char*)data),(uint32_t)data);  
    }
    
    
    uint8_t counter = 0;
    cmd_type = data[0] + data[1]*256;
    
    switch(cmd_type)    
    {
        case ('W'+'?'*256):  
          sendWL(10);
          break;
          
        case ('F'+'C'*256):  
            sd_flash_page_erase(127);
            sprintf(buff,"CF:TB%02X%02X Flash memory cleared\n",(uint8_t)deviceID[0],(uint8_t)deviceID[1]);
            uart_put_string(buff,strlen(buff));
          break;  
      
        case ('F'+'W'*256):  
            sd_flash_page_erase(127);
            writeflash_countdown = 2;
            sprintf(buff,"CF:TB%02X%02X Flash memory wrote\n",(uint8_t)deviceID[0],(uint8_t)deviceID[1]);
            uart_put_string(buff,strlen(buff));
          break;  
        
          
        case ('W'+'L'*256):
          
          WL_index = data[2] - '0';
          preferred_whitelist[WL_index] = string2MAC(&data[4]);
          m_ble_add_whitelist(preferred_whitelist);                  
          sendWL(WL_index);  
          break;
        case ('W'+'M'*256):
          
          for(counter = 0; counter < 8 ; counter ++)
            preferred_whitelist[counter] = string2MAC(&data[3+(12*counter)]);
          
          m_ble_add_whitelist(preferred_whitelist);        
          sendWL(10);  
          break;  

        case ('F'+'R'*256):
           loadWLfromFlash();
           sprintf(buff,"FL:TB%02X%02X Flash load success\n",(uint8_t)deviceID[0],(uint8_t)deviceID[1]);
           uart_put_string(buff,strlen(buff)); 
        
        case ('F'+'U'*256):
          sd_flash_page_erase(127);
          for(counter = 0; counter < 8 ; counter ++)
            preferred_whitelist[counter] = string2MAC(&data[3+(12*counter)]);          
          m_ble_add_whitelist(preferred_whitelist);
          sendWL(10);            
          break;  
          
        case ('H'+'B'*256):
          if(data[4] == 0x0A)
          {
            _hb_countdown = (int8_t)(data[3]-'0');  
          }else if(data[5] == 0x0A)
          {
            _hb_countdown = (int8_t)(data[3]-'0')*10 + (int8_t)(data[4]-'0');  
          }
          if(_hb_countdown <1 )
          {
            _hb_countdown = 1;
          }else if(_hb_countdown >60)
          {
            _hb_countdown = 60;
          }
          hb_countdown = 0;
          sprintf(buff,"HB:TB%02X%02X OK countdown = %d\n",(uint8_t)deviceID[0],(uint8_t)deviceID[1],_hb_countdown);
          uart_put_string(buff,strlen(buff)); 
          break;
        case ('O'+'K'*256): 
          //NRF_LOG_INFO("Ack recieved clear wait flag\r\n");
          is_NB_IOT_wait_ack = 0;
          break;
        case ('E'+'R'*256): 
          //NRF_LOG_INFO("Ack recieved clear wait flag\r\n");
          is_NB_IOT_wait_ack = 0;
          break;        
        case ('S'+'E'*256):  
          break;        
        case ('P'+'R'*256):  
          break;        
        case ('A'+'P'*256):  
          break;        
        case ('R'+'A'*256):  
          break;        
        case ('\r'+'\n'*256):  
          break;
        case ('+'+'N'*256):
          break;
        case ('+'+'C'*256):     
          memcpy(&buff,&data[0],6);
          buff[8] = 0;
          if(0==strcmp((char*)buff,"+CGATT"))
          {
            if(data[7] == '1')
            {              
              //NRF_LOG_INFO("NBIOT ready\r\n");
              nb_CGATT_1();
            }else
            {
              NRF_LOG_INFO("NBIOT not ready\r\n");
              nb_CGATT_0();
            }
          }
          if(0==strcmp((char*)buff,"+CGSN:"))
          {  
            memcpy(&serialNO,&data[6],15);
            NRF_LOG_INFO("SERIAL NO = %s\r\n",(uint32_t)serialNO);
          }
          break;
        case ('0'+','*256): // nb reply command 
          if(strlen((char *)data)>10)
            nb_cmd_intrepred(data);
        break;
        
        case ('t'+'s'*256):
          NRF_LOG_INFO("\r\n====================== Time syncronized ====================== \r\n");
          current_time = hexStringToTime((char*)(&data[2]));
          break;
        
        default:
          NRF_LOG_INFO("Invalid command\r\n");
          
          break;
    }    

}

// ========================================= NB IOT Machine ====================================================

#define nb_wait_timeout  5
uint8_t nb_ready_to_send = 0;

#define nb_STATE_INIT           0
#define nb_STATE_CFUN           1
#define nb_STATE_CGSN           2
#define nb_STATE_CGMR           3
#define nb_STATE_CIMI           4
#define nb_STATE_CGATT1         5
#define nb_STATE_NCONFIG        6
#define nb_STATE_CGATT_         7
#define nb_STATE_NSOCR          8
#define nb_STATE_READY_TO_SEND  9
#define nb_STATE_CGPADDR        10
#define nb_STATE_NPING          11
#define nb_STATE_READY_TO_RECV  12
#define nb_STATE_READY_TO_RECV_WAIT 13
uint8_t nbState = 0;

void nb_CGATT_1(void)
{
  nb_ready_to_send = 1;
}
void nb_CGATT_0(void)
{
  nb_ready_to_send = 0;
}

uint8_t batt=0;
uint8_t hr=0;
uint16_t temp = 3000;


uint16_t temperature_cur = 2010;
uint8_t battery_cur = 0;
uint8_t rssi_cur = 0;
void getdata(char *data)
{
  

  battery_cur++;
  if(battery_cur == 100)
  {
    battery_cur = 0;
  }


  
  char hexstring_byte[3];
  uint16_t cmd_lenght;
  uint8_t counter = 0;  
  
  
  
  char ieme_string[31] = "";  
  cmd_lenght = strlen(serialNO);
  for(counter = 0; counter<cmd_lenght;counter++)
  {
    sprintf(hexstring_byte,"%02x",serialNO[counter]);
    strcat(ieme_string,hexstring_byte);        
  }
  
  
  char len_string[3] = "0f";
  char time_stamp_string[9] = "5ba1f7bc";  
  char data_type_string[3] = "01";
  char mac_string[13] = "123456789012";    
  
  uint8_t target_addr[6];           
                 target_addr[0] = preferred_whitelist[mapping[con_handle]].addr[0];
                 target_addr[1] = preferred_whitelist[mapping[con_handle]].addr[1];
                 target_addr[2] = preferred_whitelist[mapping[con_handle]].addr[2];
                 target_addr[3] = preferred_whitelist[mapping[con_handle]].addr[3];
                 target_addr[4] = preferred_whitelist[mapping[con_handle]].addr[4];
                 target_addr[5] = preferred_whitelist[mapping[con_handle]].addr[5];
  
  sprintf(mac_string,"%02x%02x%02x%02x%02x%02x",
                  target_addr[0],
                  target_addr[1],
                  target_addr[2],
                  target_addr[3],
                  target_addr[4],
                  target_addr[5]    );
  
  char rssi_string[3] = "00";
  sprintf(rssi_string,"%02x",rssi_cur);
  
  
  char batt_string[3] = "00";
  sprintf(batt_string,"%02x",battery_cur);
  
  
  char temp_string[5] = "0000";
  uint8_t temp_high;  
  uint8_t temp_low;  
  temp_high = temperature_cur / 100;
  temp_low = temperature_cur % 100;
  sprintf(temp_string,"%02x%02x",temp_high,temp_low);
  
  char checksum_string[5] = "aaaa";
  
  
  char t_data[100];
  
  sprintf(t_data,"send:%s%s%s%s%s%s%s%s%s",
          ieme_string,
          len_string,
          time_stamp_string,
          data_type_string,
          mac_string,
          rssi_string,          
          batt_string,
          temp_string,
          checksum_string);
  
  data[0] = 0;

  cmd_lenght = strlen(t_data);
  for(counter = 0; counter<cmd_lenght;counter++)
  {
    sprintf(hexstring_byte,"%02x",t_data[counter]);
    strcat(data,hexstring_byte);        
  }
  
}


uint8_t string2HexString(char *in,char *out) // must end with null terminator
{
  uint8_t counter = 0;
  char datain[255] = "";
  
  char hexstring_byte[3];  
  char OutputHexstring[255] = "";  
  uint8_t stringLenght = 0 ;
  
  strcpy(datain,in);
  
  stringLenght = strlen(datain);
  if(stringLenght > 128)
  {
    NRF_LOG_INFO("\r\n ERROR overflow \r\n");
  }
  
  for(counter = 0; counter<stringLenght;counter++)
  {
    sprintf(hexstring_byte,"%02x",datain[counter]);
    strcat(OutputHexstring,hexstring_byte);        
  }
  
  strcpy(out,OutputHexstring);
  return(counter); 
}

time_t hexStringToTime(char* hextime)
{
    long timeVal = 0;    
    timeVal = strtol(hextime, NULL, 16);
    return((time_t)timeVal);
}

void gettime2(char *data)
{
  string2HexString("tsync:",data);
  
  //7473796e633a
}

char* targetIP = "35.186.146.40";
char* targetPORT = "4936";


uint8_t nbIoTMachine()
{
  char buff[256];
  char buff2[256];
  char* c_time_string;
  c_time_string = ctime(&current_time);
  c_time_string[strlen(c_time_string)-1] = 0;
  if(is_NB_IOT_wait_ack == 0)
  {
    //NRF_LOG_INFO("nbState = cleared\r\n",nbState);
  }
  else if (is_NB_IOT_wait_ack == 1)
  {
    //NRF_LOG_INFO("nbState = %d wait not send command\r\n",nbState);
  }else if (is_NB_IOT_wait_ack <= nb_wait_timeout)
  {
    //NRF_LOG_INFO("nbState = %d wait for %d times\r\n",nbState,is_NB_IOT_wait_ack-1);
  }else 
  {
    //NRF_LOG_INFO("nbState = %d resend command\r\n",nbState);
    is_NB_IOT_wait_ack = 1;
  }
    
  
  switch(nbState)
  {
  case nb_STATE_INIT:
    if(is_NB_IOT_wait_ack == 0) // go next state
    {
      is_NB_IOT_wait_ack = 1;
      nbState=nb_STATE_CFUN; 
    }else if (is_NB_IOT_wait_ack == 1) // first time state send command
    {
      //NRF_LOG_INFO("nb_STATE_INIT\r\n");
      
      sprintf(buff,"AT\r\n");      
      uart_put_string(buff,strlen(buff));      
      is_NB_IOT_wait_ack = 2;
    }else
    {
      is_NB_IOT_wait_ack++;// wait till timeout      
    }
    break;

   case nb_STATE_CFUN:
    if(is_NB_IOT_wait_ack == 0) // go next state
    {
      is_NB_IOT_wait_ack = 1;
      nbState=nb_STATE_CGSN; 
    }else if (is_NB_IOT_wait_ack == 1) // first time state send command
    {
      //NRF_LOG_INFO("nb_STATE_CFUN\r\n");
      NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
      sprintf(buff,"AT+CFUN=1\r\n");
      uart_put_string(buff,strlen(buff));      
      is_NB_IOT_wait_ack = 2;
    }else
    {
      is_NB_IOT_wait_ack++;// wait till timeout      
    }
     break;
    case nb_STATE_CGSN:
    if(is_NB_IOT_wait_ack == 0) 
    {
      is_NB_IOT_wait_ack = 1;
      nbState=nb_STATE_CGMR;                    // <<< next state
    }else if (is_NB_IOT_wait_ack == 1) 
    {
      //NRF_LOG_INFO("nb_STATE_CFUN\r\n");        // << this state name
      NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
      sprintf(buff,"AT+CGSN=1\r\n");            // << this state command
      uart_put_string(buff,strlen(buff));      
      is_NB_IOT_wait_ack = 2;
    }else
    {
      is_NB_IOT_wait_ack++;// wait till timeout      
    }
     
      break;   
    case nb_STATE_CGMR:
        
    if(is_NB_IOT_wait_ack == 0) 
    {
      is_NB_IOT_wait_ack = 1;
      nbState=nb_STATE_CIMI;                    // <<< next state
    }else if (is_NB_IOT_wait_ack == 1) 
    {
      //NRF_LOG_INFO("nb_STATE_CGMR\r\n");        // << this state name
      NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
      sprintf(buff,"AT+CGMR\r\n");            // << this state command
      uart_put_string(buff,strlen(buff));      
      is_NB_IOT_wait_ack = 2;
    }else
    {
      is_NB_IOT_wait_ack++;// wait till timeout      
    }  
    break;
     
    case nb_STATE_CIMI:
            
    if(is_NB_IOT_wait_ack == 0) 
    {
      is_NB_IOT_wait_ack = 1;
      nbState=nb_STATE_CGATT1;                    // <<< next state
    }else if (is_NB_IOT_wait_ack == 1) 
    {
      //NRF_LOG_INFO("nb_STATE_CGMR\r\n");        // << this state name
      NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
      sprintf(buff,"AT+CIMI\r\n");            // << this state command
      uart_put_string(buff,strlen(buff));      
      is_NB_IOT_wait_ack = 2;
    }else
    {
      is_NB_IOT_wait_ack++;   
    }    
    break;   
    case nb_STATE_CGATT1:
      if(is_NB_IOT_wait_ack == 0) 
      {
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_NCONFIG;                    // <<< next state
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_CGATT=1\r\n");        // << this state name
        NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
        sprintf(buff,"AT+CGATT=1\r\n");            // << this state command
        uart_put_string(buff,strlen(buff));      
        is_NB_IOT_wait_ack = 2;
      }else
      {
        is_NB_IOT_wait_ack++;   
      }
    break;

    case nb_STATE_NCONFIG:
      if(is_NB_IOT_wait_ack == 0) 
      {
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_NSOCR;                    // <<< next state
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_NCONFIG\r\n");        // << this state name
        NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
        sprintf(buff,"AT+NCONFIG=AUTOCONNECT,TRUE\r\n");            // << this state command
        uart_put_string(buff,strlen(buff));      
        is_NB_IOT_wait_ack = 2;
      }else
      {
        is_NB_IOT_wait_ack++;   
      }   

      break;

    case nb_STATE_NSOCR:
      if(is_NB_IOT_wait_ack == 0) 
      {
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_CGPADDR;                    // <<< next state
        
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_NSOCR\r\n");        // << this state name
        NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
        sprintf(buff,"AT+NSOCR=DGRAM,17,5000,1\r\n");            // << this state command
        uart_put_string(buff,strlen(buff));      
        is_NB_IOT_wait_ack = 2;
      }else
      {
        is_NB_IOT_wait_ack++;   
      }   
      break;
    case nb_STATE_CGPADDR:
      if(is_NB_IOT_wait_ack == 0) 
      {
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_NPING;                    // <<< next state
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_CGPADDR\r\n");        // << this state name
        NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
        sprintf(buff,"AT+CGPADDR\r\n");            // << this state command
        uart_put_string(buff,strlen(buff));      
        is_NB_IOT_wait_ack = 2;
      }else
      {
        is_NB_IOT_wait_ack++;   
      }  
      break;
    
    case nb_STATE_CGATT_:
      if(nb_ready_to_send && is_NB_IOT_wait_ack == 0) 
      {
        nb_ready_to_send = 0;
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_READY_TO_SEND;                    // <<<  next state
        
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_CGATT?\r\n");        // << this state name
        NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
        sprintf(buff,"AT+CGATT?\r\n");            // << this state command
        uart_put_string(buff,strlen(buff));      
        is_NB_IOT_wait_ack = 2;
      }else
      {
        is_NB_IOT_wait_ack++;   
      }   
    break;      
    case nb_STATE_READY_TO_SEND:
      if(is_NB_IOT_wait_ack == 0) 
      {
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_READY_TO_RECV;                    // <<<  next state
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_READY_TO_SEND\r\n");        // << this state name
        if(current_time > CheckTime )
        {
          getdata(buff2);
          
          NRF_LOG_INFO("%s>",(uint32_t)c_time_string);          
          sprintf(buff,"AT+NSOST=0,%s,%s,%d,%s\r\n",targetIP,targetPORT,strlen(buff2)/2,buff2);            // << this state command
          uart_put_string(buff,strlen(buff));      
          is_NB_IOT_wait_ack = 2;
        }else
        {
          
          NRF_LOG_INFO("====================== Time not sync ====================== \r\n");
          gettime2(buff2);          
          NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
          
          sprintf(buff,"AT+NSOST=0,%s,%s,%d,%s\r\n",targetIP,targetPORT,strlen(buff2)/2,buff2);            // << this state command
          uart_put_string(buff,strlen(buff));      
          is_NB_IOT_wait_ack = 2;
        }
        wdt_reload_register(TX_WDT); // clear watchdog
      }else
      {
        is_NB_IOT_wait_ack++;   
      } 
      break;
      
    case nb_STATE_READY_TO_RECV:  
      if(is_NB_IOT_wait_ack == 0) 
      {
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_CGATT_;                    // <<<  next state
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_READY_TO_RECV\r\n");        // << this state name
        NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
        sprintf(buff,"AT+NSORF=0,100\r\n");            // << this state command
        uart_put_string(buff,strlen(buff));      
        is_NB_IOT_wait_ack = 2;        
      }else
      {
        is_NB_IOT_wait_ack++;   
      } 
      
      
      break;
      
    case nb_STATE_NPING:
      if(is_NB_IOT_wait_ack == 0) 
      {
        is_NB_IOT_wait_ack = 1;
        nbState=nb_STATE_CGATT_;                    // <<<  next state
      }else if (is_NB_IOT_wait_ack == 1) 
      {
        //NRF_LOG_INFO("nb_STATE_NPING\r\n");        // << this state name
        NRF_LOG_INFO("%s>",(uint32_t)c_time_string);
        sprintf(buff,"AT+NPING=35.240.161.251\r\n");            // << this state command
        uart_put_string(buff,strlen(buff));      
        is_NB_IOT_wait_ack = 2;
      }else
      {
        is_NB_IOT_wait_ack++;   
      }   
    break;
    default:
      nbState = 0;
    break;
  }
  
  
  return(1);
}
// =============================================================================================================
int main(void)
{
    ret_code_t err_code;  
    char buff[256];
    uint32_t deviceID[2]; deviceID[0] = NRF_FICR->DEVICEID[0]; deviceID[1] = NRF_FICR->DEVICEID[1]; 
    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    
    main_variable_initialize();
    device_initialize();
    NRF_LOG_INFO("RESET-----------------------------------\n");
    NRF_LOG_INFO("================ Health Gateway NB-IoT ==============\r\n");
    nbState = 0;  

    preferred_whitelist[0].addr[0] = 0x2B;
    preferred_whitelist[0].addr[1] = 0x8D;
    preferred_whitelist[0].addr[2] = 0x88;
    preferred_whitelist[0].addr[3] = 0xEA;
    preferred_whitelist[0].addr[4] = 0x4A;
    preferred_whitelist[0].addr[5] = 0x88;
    
    //2B8D88EA4A88
    
    for (;;)
    {
        if(flag_200ms)
        {
          nbIoTMachine();
          flag_200ms = 0;
        }
        if(0==(rtc_var.count%50))
        {
          
        }
        if(rtc_var.sec > 0) // call every sec
        {          
           
          
          
          if(writeflash_countdown >= 0 )
          {
            if(writeflash_countdown == 0)
            {
              sd_flash_write((uint32_t*)0x7F000, (uint32_t*) &preferred_whitelist,12);
            }
            writeflash_countdown --;
            
          }
          
          if(hb_countdown <= 1)
          {
            hb_countdown = _hb_countdown;
            //NRF_LOG_INFOntf(buff,"HB:TB%02X%02X\n",(uint8_t)deviceID[0],(uint8_t)deviceID[1]);
            //sprintf(buff,"AT\r\n");
            //uart_put_string(buff,strlen(buff));
          }else
          {
            hb_countdown--;
          }
          
          
            
            rtc_var.sec = 0;
        }       
             
        if(tag_disconnect_flag)
        {
          //delete_device_list(disconnect_addr);
          sprintf(buff,"DC,TB%02X%02X,%02X%02X%02X%02X%02X%02X\n",(uint8_t)deviceID[0],(uint8_t)deviceID[1],disconnect_addr[0],disconnect_addr[1],disconnect_addr[2],disconnect_addr[3],disconnect_addr[4],disconnect_addr[5]);
          uart_put_string(buff,strlen(buff));
          nrf_delay_ms(5);
          tag_disconnect_flag = 0;
        }
        if(remote_cmd_flag)
        {           
          remote_cmd_process(&remote_cmd_data[0]);
          remote_cmd_flag = 0;
        }
        //whitelist management
        if (1)
        {
              if(rssi_data_value != 0 )
              {
                 uint8_t target_addr[6]; 
                 target_addr[0] = preferred_whitelist[mapping[rssi_data_conn_handle]].addr[0];
                 target_addr[1] = preferred_whitelist[mapping[rssi_data_conn_handle]].addr[1];
                 target_addr[2] = preferred_whitelist[mapping[rssi_data_conn_handle]].addr[2];
                 target_addr[3] = preferred_whitelist[mapping[rssi_data_conn_handle]].addr[3];
                 target_addr[4] = preferred_whitelist[mapping[rssi_data_conn_handle]].addr[4];
                 target_addr[5] = preferred_whitelist[mapping[rssi_data_conn_handle]].addr[5];
                  sprintf(buff,"RI:TB%02X%02X,%d %02X%02X%02X%02X%02X%02X \n",(uint8_t)deviceID[0],(uint8_t)deviceID[1],rssi_data_value,target_addr[0],
                  target_addr[1],
                  target_addr[2],
                  target_addr[3],
                  target_addr[4],
                  target_addr[5]);
                  
                  rssi_cur = (rssi_data_value * -1);
                  
                  //uart_put_string(buff,strlen(buff));
                  NRF_LOG_INFO("%s",(uint32_t)buff)
                
                rssi_data_value = 0;
              }

              if(temp_data[0] != 0)
              {
                 
                 uint8_t target_addr[6];           
                 target_addr[0] = preferred_whitelist[mapping[con_handle]].addr[0];
                 target_addr[1] = preferred_whitelist[mapping[con_handle]].addr[1];
                 target_addr[2] = preferred_whitelist[mapping[con_handle]].addr[2];
                 target_addr[3] = preferred_whitelist[mapping[con_handle]].addr[3];
                 target_addr[4] = preferred_whitelist[mapping[con_handle]].addr[4];
                 target_addr[5] = preferred_whitelist[mapping[con_handle]].addr[5];
              
                
                  temperature_cur = temp_data[5]*256 + temp_data[4];
                  
                  sprintf(buff,"{\"version\":\"1.0.0\",\"ts\":\"000000000000000\",\"src\":{\"scc\":1,\"sn\":\"TB%02X%02X\"},\"mthd\":1,\"tpc\":0,\"cont\":[{\"scc\":21,\"sn\":\"%02X%02X%02X%02X%02X%02X\",\"rssi\":0,\"data\":\"%02X%02X%02X%02X%02X%02X\"}]}\r\n",
                  (uint8_t)deviceID[0],(uint8_t)deviceID[1],
                  target_addr[0],
                  target_addr[1],
                  target_addr[2],
                  target_addr[3],
                  target_addr[4],
                  target_addr[5],                  
                  temp_data[1],
                  temp_data[2],
                  temp_data[3],
                  temp_data[4],
                  temp_data[5],
                  temp_data[6]);
                  //uart_put_string(buff,strlen(buff));
                  NRF_LOG_INFO("%s",(uint32_t)buff)
                  wdt_reload_register(TX_WDT);
                  temp_data[0] = 0;

              }
            
            // Wait for BLE events.
            if(NRF_LOG_PROCESS()==false)
            {
              power_manage();
            }
        }
    }
}
